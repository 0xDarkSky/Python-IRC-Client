# Python-IRC-Client

Simple Python IRC Client with SSL support.

# Features

- Send messages to IRC channel
- Use SSL
- Read messages from IRC channel
- Read and send at the same time with Threading
- Log channel messages to file

# How To Use?

Make sure you have `python3` installed. 
The code works on Linux, haven't tested on Windows.

Setup the `config.json` file and run `main.py`.

```
{
    "server":"some.irc.server",
    "port":6697,
    "channel": "#somechannel",
    "botnick":"nickname of your account",
    "botpassw":"your account password"
    "log_to_file":"true or false"
}
```

# TO-DO

- Add more documentation and check for bugs in the `v2_release`

# Disclaimer

This project is not yet a useable IRC client. It works, but I haven't tested it enough and it could be improved. You can use it to create an IRC bot or implement it somewhere else.
